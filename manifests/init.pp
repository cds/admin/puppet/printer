class printer (
    ){
        if $facts['os']['family'] == 'RedHat' {

	file { '/etc/cups/printers.conf.staging':
	    ensure => file,
	    mode => '0644',
	    owner => 'root',
	    group => 'lp',
	    source => ["puppet://puppet/modules/printer/etc/cups/printers.conf"],
	    notify => Exec['reload-cups'],
	}
        file { '/etc/cups/ppd/color-51.ppd':
	    ensure => file,
	    owner => 'root',
	    group => 'lp',
	    mode => '0644',
	    source => ["puppet://puppet/modules/printer/etc/cups/ppd/color-51.ppd"],
	    notify => Exec['reload-cups'],
	}
        file { '/etc/cups/ppd/color-GC.ppd':
	    ensure => file,
	    owner => 'root',
	    group => 'lp',
	    mode => '0644',
	    source => ["puppet://puppet/modules/printer/etc/cups/ppd/color-GC.ppd"],
	    notify => Exec['reload-cups'],
	}
        file { '/etc/cups/ppd/color-front.ppd':
	    ensure => file,
	    owner => 'root',
	    group => 'lp',
	    mode => '0644',
	    source => ["puppet://puppet/modules/printer/etc/cups/ppd/color-front.ppd"],
	    notify => Exec['reload-cups'],
	}
        file { '/etc/cups/ppd/color-control.ppd':
	    ensure => file,
	    owner => 'root',
	    group => 'lp',
	    mode => '0644',
	    source => ["puppet://puppet/modules/printer/etc/cups/ppd/color-control.ppd"],
	    notify => Exec['reload-cups'],
	}

	exec { 'reload-cups':
	    command => '/bin/systemctl stop cups ; cp /etc/cups/printers.conf.staging /etc/cups/printers.conf ; /bin/systemctl start cups' ,
	    refreshonly => true,
	}
    }
}
